# Code 128 Barcode Generator

code128generator(data, canvas, printText)

Returns a string of barcode drawing elements,where`1`represents a bar and`0`represents a space.

The`data`parameter is the data needed to generate the barcode,which is a string that can contain all characters that Code 128 can include.

The`canvas`parameter is a canvas element.If provided,the barcode will be drawn within this element;otherwise,no drawing will be performed.

The`printText`parameter is a boolean value,which is not mandatory.If the`canvas`parameter is passed,this parameter indicates whether to include barcode text when drawing the barcode;otherwise,this parameter is meaningless.